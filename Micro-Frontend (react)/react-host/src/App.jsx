import React, { useState } from "react";
import ReactDOM from "react-dom";

import Counter from "remoteCounter/Counter";
import useSessionStorage from "remoteCounter/useSessionStorage";

import "./index.scss";

const App = () => {
  const [count, setCount] = useSessionStorage("COUNTER-REACT-HOST", 0);
  return (
    <div className="mt-10 text-3xl mx-auto max-w-6xl">
      <div>Name: react-host</div>
      <Counter count={count} setCount={setCount} />
    </div>
  );
};
ReactDOM.render(<App />, document.getElementById("app"));
