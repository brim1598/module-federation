import React, { useState } from "react";
import ReactDOM from "react-dom";

import Counter from "remoteCounter/Counter";

import "./index.scss";

const App = () => {
  const [count, setCount] = useState(0);
  return (
    <div class="mt-10 text-3xl mx-auto max-w-6xl">
      <div>Name: remote</div>
      <Counter count={count} setCount={setCount} />
    </div>
  );
};
ReactDOM.render(<App />, document.getElementById("app"));
