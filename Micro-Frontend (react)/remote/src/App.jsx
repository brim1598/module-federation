import React from "react";
import ReactDOM from "react-dom";
import Counter from "./Counter";
import useSessionStorage from "./useSessionStorage";
import "./index.scss";

const App = () => {
  const [count, setCount] = useSessionStorage("COUNTER", 0);
  return (
    <div class="mt-10 text-3xl mx-auto max-w-6xl">
      <div>Name: remote</div>
      <Counter count={count} setCount={setCount} />
    </div>
  );
};
ReactDOM.render(<App />, document.getElementById("app"));
