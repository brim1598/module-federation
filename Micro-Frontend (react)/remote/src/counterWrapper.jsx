import { render } from "react-dom";
import Counter from "./Counter";

export default (el) => {
  render(Counter, el);
};
