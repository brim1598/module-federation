import React from "react";

export default ({ count, setCount }) => {
  return (
    <div class="bg-blue-900 text-white p-5">
      <div>Count = {count}</div>
      <button onClick={() => setCount(count + 1)}>Add One</button>
    </div>
  );
};
